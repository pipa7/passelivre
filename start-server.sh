#!/bin/bash
serverPidFile='tmp/pids/server.pid'

if [ -f "$serverPidFile" ]
then
    rm "$serverPidFile"
fi

bundle exec rails s -p 3000 -b '0.0.0.0'