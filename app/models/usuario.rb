class Usuario < ActiveRecord::Base
	before_save { self.nome = nome.downcase }

	validates :nome, presence: true, length: { maximum: 25 },
		uniqueness: { case_sensitive: false }
	validates :nome_completo, presence: true

	has_secure_password
	validates :password_confirmation, presence: true
	validates :password, length: { minimum: 4 }
end
