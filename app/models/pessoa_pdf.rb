# encoding: UTF-8

class PessoaPdf < Prawn::Document
	require "prawn/measurement_extensions"

	def initialize(pessoa, view)
		super(page_size: "A4")
		@pessoa = pessoa
		@view = view
		#text "Cadastro do Passe Livre"
		#text "Nome: #{@pessoa.nome}"
		credencial
	end

	def credencial
		self.line_width = 1

		bounding_box([0, 265.mm], width: 180.mm, height: 65.mm) do
			transparent (0.4) {
				image "app/assets/images/brasao.png", height: 63.mm, at: [20.mm, 64.mm]
			}
			transparent (0.3) {
				image "app/assets/images/brasao.png", height: 63.mm, at: [100.mm, 64.mm]
			}
			stroke_bounds
			stroke_vertical_line 0, 65.mm, at: 90.mm



			bounding_box [2.mm, 63.mm], width: 30.mm, height: 40.mm do
				transparent(0.5) { stroke_bounds }
				text "Foto 3x4", align: :center, valign: :center, size: 8
			end

			bounding_box [35.mm, 63.mm], width: 55.mm, height: 40.mm do
				text "Estado do Rio Grande do Sul", size: 10, style: :bold
				text "Prefeitura Municipal de Imbé", size: 10
				text "Secretaria de Ação Social", size: 10, style: :italic
				move_down 8.mm
				text "PASSE LIVRE", size: 14, style: :bold, align: :center
				text "Lei Nº 771/03", size: 12, style: :bold, align: :center
			end
			font ("Times-Roman") do
				text_box "PESSOA PORTADORA DE DEFICIÊNCIA",
					at: [2.mm ,21.mm],
					width: 86.mm,
					height: 4.mm,
					overflow: :shrink_to_fit,
					align: :center,
					valign: :center,
					size: 16,
					style: :bold
			end

			stroke_rectangle [2.mm, 16.mm], 86.mm, 7.mm
			draw_text "Nome", size: 6, at: [2.mm + 1, 14.mm]
			text_box @pessoa.nome, size: 10, style: :bold,
				at: [5.mm, 13.mm], width: 80.mm, height: 4.mm

			if @pessoa.acompanhante?
				text_box @pessoa.acomp_nome,
					size:14, style: :bold, align: :center,
					valign: :center, overflow: :shrink_to_fit,
					at: [2.mm, 7.mm], width: 86.mm, height: 6.mm
			end



			####Verso
			#Primeira linha
			stroke_rectangle [93.mm, 62.mm], 84.mm, 8.mm
			draw_text "Endereço", size: 6, at: [93.mm + 1, 60.mm]
			text_box "#{@pessoa.endereco}, #{@pessoa.numero}", size: 10, style: :bold,
				at: [96.mm, 58.mm], width: 78.mm, height: 4.mm

			#Segunda linha
			stroke_rectangle [93.mm, 52.mm], 72.mm, 8.mm
			draw_text "Município", size: 6, at: [93.mm + 1, 50.mm]
			text_box @pessoa.cidade, size: 10, style: :bold,
				at: [96.mm, 48.mm], width: 66.mm, height: 4.mm

			stroke_rectangle [167.mm, 52.mm], 10.mm, 8.mm
			draw_text "UF", size: 6, at: [167.mm + 1, 50.mm]
			text_box @pessoa.estado, size: 10, style: :bold,
				at: [170.mm, 48.mm], width: 8.mm, height: 4.mm

			#Terceira linha
			stroke_rectangle [93.mm, 42.mm], 26.mm, 8.mm
			draw_text "RG", size: 6, at: [93.mm + 1, 40.mm]
			text_box @pessoa.rg, size: 10, style: :bold,
				at: [96.mm, 38.mm], width: 20.mm, height: 4.mm

			stroke_rectangle [121.mm, 42.mm], 24.mm, 8.mm
			draw_text "Data de Nasc.", size: 6, at: [121.mm + 1, 40.mm]
			text_box @pessoa.data_nasc.to_s(:data_abreviada), size: 10, style: :bold,
				at: [125.mm, 38.mm], width: 18.mm, height: 4.mm

			stroke_rectangle [147.mm, 42.mm], 30.mm, 8.mm
			draw_text "CPF", size: 6, at: [147.mm + 1, 40.mm]
			text_box @pessoa.cpf, size: 10, style: :bold,
				at: [150.mm, 38.mm], width: 26.mm, height: 4.mm


			#Quarta Linha
			stroke_rectangle [93.mm, 32.mm], 32.mm, 8.mm
			draw_text "Telefone", size: 6, at: [93.mm + 1, 30.mm]
			text_box @pessoa.telefone, size: 10, style: :bold,
				at: [96.mm, 28.mm], width: 26.mm, height: 4.mm

			stroke_rectangle [127.mm, 32.mm], 24.mm, 8.mm
			draw_text "Emitido em", size: 6, at: [127.mm + 1, 30.mm]
			text_box @pessoa.updated_at.to_s(:data_abreviada), size: 10, style: :bold,
				at: [130.mm, 28.mm], width: 18.mm, height: 4.mm

			stroke_rectangle [153.mm, 32.mm], 24.mm, 8.mm
			draw_text "Válido até", size: 6, at: [153.mm + 1, 30.mm]
			text_box @pessoa.getDataExpiracao, size: 10, style: :bold,
				at: [156.mm, 28.mm], width: 18.mm, height: 4.mm

			#Assinaturas
			stroke_horizontal_line 93.mm, 133.mm, at: 8.mm
			stroke_horizontal_line 137.mm, 177.mm, at: 8.mm
			text_box "Ass. Portador",
				at: [93.mm, 7.mm],
				width: 40.mm,
				height: 4.mm,
				overflow: :shrink_to_fit,
				align: :center,
				size: 8
			text_box "Ass. Prefeito",
				at: [137.mm, 7.mm],
				width: 40.mm,
				height: 4.mm,
				overflow: :shrink_to_fit,
				align: :center,
				size: 8
		end

		move_down 15.mm


		###FICHA DO CADASTRO

		bounding_box [0, 185.mm], width: 180.mm, height: 170.mm do

			font "Times-Roman"

			image "app/assets/images/brasao.png", height: 30.mm

			bounding_box [40.mm, 170.mm], width: 140.mm do
				text "ESTADO DO RIO GRANDE DO SUL", size: 12, style: :bold
				text "PREFEITURA MUNICIPAL DE IMBÉ", size: 12
				text "SECRETARIA MUN. DE AÇÃO SOCIAL", size: 12, style: :italic
				text "Av. Nova Pretrópolis, xxx"
				text "Imbé/RS CEP 95.625-000"
				text "Telefone: (51)3627-4405"
			end

			move_down 8.mm
			text "CADASTRO PARA PASSE LIVRE", style: :bold, align: :center
			text "PESSOA PORTADORA DE DEFICIÊNCIA - PPD", style: :bold, align: :center
			text "Lei Mun. 771/03", style: :bold, align: :center

			move_down 10.mm
			default_leading 5

			text "<b>Nome:</b> #{@pessoa.nome}", inline_format: true
			text "<b>Data de Nascimento:</b> #{@pessoa.data_nasc.to_s(:data_abreviada)}", inline_format: true
			text "<b>RG:</b> #{@pessoa.rg}", inline_format: true
			text "<b>CPF:</b> #{@pessoa.cpf}", inline_format: true
			text "<b>Nome do Pai:</b> #{@pessoa.pai}", inline_format: true
			text "<b>Nome da Mãe:</b> #{@pessoa.mae}", inline_format: true
			text "<b>Endereço:</b> #{@pessoa.endereco}, #{@pessoa.numero} - #{@pessoa.complemento}", inline_format: true
			text "<b>Telefone:</b> #{@pessoa.telefone}", inline_format: true
			text "<b>CID:</b> #{@pessoa.cid}", inline_format: true
			text "<b>Caracterização da Deficiência:</b> #{@pessoa.descr_deficiencia}", inline_format: true
			text "<b>Laudo Médico Emitido Por:</b> #{@pessoa.emissor_laudo}", inline_format: true
			text "<b>Data do Laudo:</b> #{@pessoa.data_laudo.to_s(:data_abreviada)}", inline_format: true
			text "<b>Emissão do Passe Livre:</b> #{@pessoa.updated_at.to_s(:data_abreviada)}", inline_format: true
			text "<b>Valido até:</b> #{@pessoa.getDataExpiracao}", inline_format: true
			text "<b>Direito a acompanhante:</b> #{(@pessoa.acompanhante? ? '<u>Sim</u>' : 'Não' )}", inline_format: true

			if @pessoa.acompanhante?
				text "<b>Nome acompanhante:</b> #{@pessoa.acomp_nome}", inline_format: true
			end

		end

		bounding_box [125.mm, 10.mm], width: 50.mm, height: 10.mm do
			stroke_horizontal_line 0, 50.mm, at: 10.mm
			move_down 2.mm
			text "Ass. Portador", align: :center
		end






	end

end