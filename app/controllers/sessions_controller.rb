# encoding: UTF-8

class SessionsController < ApplicationController
	skip_before_action :usuario_logado
	def new

	end

	def create
		usuario = Usuario.find_by(nome: params[:session][:nome].downcase)
		if usuario && usuario.authenticate(params[:session][:password])
			loga usuario
			redirect_to pessoas_path
		else
			flash.now[:error] = 'Usuario não existe ou senha incorreta.'
			render 'new'
		end
	end

	def destroy
		desloga
		redirect_to root_url
	end
end
