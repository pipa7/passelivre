class AddDataExpiracaoToPessoas < ActiveRecord::Migration
  def change
    add_column :pessoas, :data_expiracao, :date
  end
end
